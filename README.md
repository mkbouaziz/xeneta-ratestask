# Submission
This repository is the submission of the [task](https://github.com/xeneta/ratestask).  

Notes:  

- The framework used to develop the APIs is Django, as it's the Framework I am expert with, and I will be sure to deliver a code can be used in production. I didn't used Flask before, but I tried it during the task and it seems to be more simple than Django.
- I restructured the docker architecture, to use Docker Compose, cleaner and more simple to setup. And I created the needed files in the run in production mode.
- The rates.sql file should be added to Git with Git LFS, but I didn't add it to make the setup the easiest possible, as it's a small task. But in the work environment, every big file should be added to Git LFS.
- Django use ORM to deal with databases, but as requested, I didn't use it, and just used SQL raw queries.
- The time spent to realise the project was around 4 hours, the majority of time spent was in SQL, because I didn't use SQL for a while, maybe the ORM of Django facilitated many things, but it wasn't hard at all.

# Setup

You only need to run:
```bash
docker-compose up
```

The Unit Tests will be run also to show the result of them
```
.
---------------------------------------------------------------------
Ran 1 test in 0.035s
OK
```
You can run the curl command:
```bash
curl "http://127.0.0.1/rates?date_from=2016-01-01&date_to=2016-01-10&origin=CNSGH&destination=north_europe_main"
```
To get the result
```json
[
   {
      "day":"2016-01-01",
      "average_price":1112
   },
   {
      "day":"2016-01-02",
      "average_price":1112
   },
   {
      "day":"2016-01-03",
      "average_price":null
   },
   {
      "day":"2016-01-04",
      "average_price":null
   },
   {
      "day":"2016-01-05",
      "average_price":1142
   },
   {
      "day":"2016-01-06",
      "average_price":1142
   },
   {
      "day":"2016-01-07",
      "average_price":1137
   },
   {
      "day":"2016-01-08",
      "average_price":1124
   },
   {
      "day":"2016-01-09",
      "average_price":1124
   },
   {
      "day":"2016-01-10",
      "average_price":1124
   }
]
```