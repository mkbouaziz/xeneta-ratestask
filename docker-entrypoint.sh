#!/bin/bash
if [ ! -e env ]
then
  python -m venv env
fi
source env/bin/activate

pip3 install -r requirements.txt
python manage.py makemigrations --no-input
python manage.py migrate --no-input
python manage.py collectstatic --noinput
python manage.py test
# Run WSGI with Apache
mod_wsgi-express start-server api/wsgi.py --port=80 --user www-data --group www-data