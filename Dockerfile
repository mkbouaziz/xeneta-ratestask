FROM python:3.9-slim-buster

ENV PYTHONUNBUFFERED 1
ENV PYTHONDONTWRITEBYTECODE 1
ENV SHELL /bin/bash

RUN apt-get update \
  # install Apache2
  && apt-get install -y libapache2-mod-wsgi-py3 apache2 apache2-utils apache2-dev \
  # cleaning up unused files
  && rm -rf /var/lib/apt/lists/* && rm -rf /root/.cache

ENV PYTHONIOENCODING utf-8

ENV APACHE_LOG_DIR /var/log/apache2

RUN echo "WSGIPassAuthorization On" >> /etc/apache2/apache2.conf &&\
  echo "WSGIApplicationGroup %{GLOBAL}" >> /etc/apache2/apache2.conf &&\
  echo "ServerName localhost" >> /etc/apache2/apache2.conf &&\
  service apache2 restart

EXPOSE 80

RUN mkdir -p /home/docker/api/

WORKDIR /home/docker/api

ENTRYPOINT ["/home/docker/docker-entrypoint.sh"]
