# -*- coding: utf-8 -*-
from django.db import connection

def dictfetchall(cursor):
    """
    Return all rows from a cursor as a dict
    """
    columns = [col[0] for col in cursor.description]
    return [dict(zip(columns, row)) for row in cursor.fetchall()]

def get_prices(origin, destination,date_from,date_to):
    """Get Prices rates

    Keyword arguments:
    origin -- The origin port code of region slug
    destination -- The destination port code of region slug
    date_from -- Date from
    date_to -- Date to
    """
    with connection.cursor() as cursor:
        cursor.execute("""
            WITH RECURSIVE 
            origin_regions AS ( -- recursive slug origin regions
                SELECT
                    slug,name,parent_slug
                FROM
                    regions
                WHERE
                    parent_slug = %s
                UNION
                    SELECT
                        r.slug,r.name,r.parent_slug
                    FROM
                        regions r
                    INNER JOIN origin_regions s ON s.slug = r.parent_slug
                ),
            origin_ports AS ( -- search ports with origin regions
                SELECT
                    po.code
                FROM
                    origin_regions AS s
                INNER JOIN ports AS po ON po.parent_slug=s.slug
                ),
            destination_regions AS ( -- recursive slug destination regions
                SELECT
                    slug,name,parent_slug
                FROM
                    regions
                WHERE
                    parent_slug = %s
                UNION
                    SELECT
                        r.slug,r.name,r.parent_slug
                    FROM
                        regions r
                    INNER JOIN destination_regions s ON s.slug = r.parent_slug
                ),
            destination_ports AS ( -- search ports with destination regions
                SELECT
                    po.code
                FROM
                    destination_regions AS s
                INNER JOIN ports AS po ON po.parent_slug=s.slug
                ),
            dates AS ( -- generate dates of period
                SELECT day
                FROM (SELECT generate_series(DATE %s, DATE %s, '1 day')::date AS day) x
                ),
            rates AS ( -- main, get rates with search and aggregation
                SELECT day,round(avg(price)) AS average_price
                FROM prices AS pr
                INNER JOIN ports AS po1 ON pr.orig_code = po1.code
                INNER JOIN ports AS po2 ON pr.dest_code = po2.code
                LEFT JOIN regions AS r1 ON po1.parent_slug = r1.parent_slug
                LEFT JOIN regions AS r2 ON po2.parent_slug = r2.parent_slug
                WHERE   (pr.orig_code=%s OR r1.slug=%s OR r1.parent_slug=%s OR (pr.orig_code IN (SELECT * from origin_ports))) 
                    AND (pr.dest_code=%s OR r2.slug=%s OR r2.parent_slug=%s OR (pr.dest_code IN (SELECT * from destination_ports))) 
                    AND pr.day BETWEEN DATE %s AND DATE %s
                GROUP BY day
                    HAVING COUNT(price) >=3 
                ORDER BY day ASC
                )
            SELECT d.day , r.average_price average_price
            FROM dates d
            LEFT JOIN rates r ON d.day = r.day
            ORDER BY d.day ASC
            """,[origin,destination,date_from,date_to,origin,origin,origin,destination,destination,destination,date_from,date_to])
        return dictfetchall(cursor)
