# -*- coding: utf-8 -*-
from rest_framework import serializers

class RatesSerializerIn(serializers.Serializer):
    """
    Serializer for GET Parameters, to get Daily Rates
    """
    date_from = serializers.DateField()
    date_to = serializers.DateField()
    origin = serializers.CharField()
    destination = serializers.CharField()


class RatesSerializerOut(serializers.Serializer):
    """
    Serializer for result of Daily Rates
    """
    day = serializers.CharField()
    average_price = serializers.IntegerField()

