# -*- coding: utf-8 -*-
from django.shortcuts import render
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from main.sql import *
from main.serializers import *
# Create your views here.

@api_view(['GET'])
def rates(request):
    """
    Get daily rates
    """
    if request.method == 'GET':
        serializer = RatesSerializerIn(data=request.GET)
        if serializer.is_valid():
            data = serializer.validated_data
            res = get_prices(origin=data['origin'], destination=data['destination'],date_from=str(data['date_from']),date_to=str(data['date_to']))
            return Response(RatesSerializerOut(res,many=True).data, status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

