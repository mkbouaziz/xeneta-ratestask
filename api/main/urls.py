# -*- coding: utf-8 -*-
from django.urls import path, include, re_path
from main.views import *

app_name = 'main'

urlpatterns = [    
    path('rates', rates, name='rates'),
    ]