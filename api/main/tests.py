from django.test import TestCase

# Create your tests here.
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

class RatesTests(APITestCase):
    def test_rates(self):
        """
        Ensure we can get list of daily rates.
        """
        url = reverse('main:rates')
        params = {'date_from':'2016-01-01', 'date_to':'2016-01-10','origin':'CNSGH','destination':'north_europe_main'}
        response = self.client.get(url, params, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        assert isinstance(response.json(),list) # check the result is a list
        for data in response.json():
            assert isinstance(data,dict)
            # check the exact keys
            self.assertEqual(set(data.keys()), set(['day', 'average_price']))
            # check the type of 'average_price'
            assert bool(isinstance(data['average_price'],int) or data['average_price'] is None)
            